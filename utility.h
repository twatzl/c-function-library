/*-----------------------------------------------------------
 * File:		utility.h
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Header file for utility library
 * ----------------------------------------------------------
 */

#ifndef ___UTILITY_H
#define ___UTILITY_H

#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

typedef enum boolean
{
	False,
	True
}Bool;

typedef struct data_package
{
	int key;
	void* data;
} Data_Package;

typedef char* string;

void* salloc(size_t size);

int power(int base, int exponent);

double square(double number);

double pythagoras(double a, double b);

Bool is_number(char ch);

void microsleep(int time);

//void switch_items(void* ptr1, void* ptr2);

// String utils

string new_string(int length);

int split_string(string str, char ch, string** return_array);

void replace_chars(string str, char char_to_be_replaced, char char_to_replace);

string* add_new_line(string* array, int act_number_of_lines, string line);

int convert_string_to_int(string str);

string convert_int_to_string(int i);

int convert_char_to_int(char ch);

char convert_int_to_char(int i);

#endif