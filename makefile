CC		= gcc
CCLINK		= g++
LIBTARGET	= libutility.a
LIBS		= 
CCOPTIONS	= -Wall -pedantic -std=gnu99 -g
LDOPTIONS	=
HDRS		=  file_utility.h sorting_algorithms.h utility.h term_utility.h linked_list.h
PROGRAM		= 
OBJS		=  file_utility.o sorting_algorithms.o utility.o term_utility.o linked_list.o

DOXY		= /Applications/Doxygen/Doxygen.app/Contents/Resources/doxygen

$(LIBTARGET): $(OBJS)
	ar rs $(LIBTARGET) $(OBJS)


clean:
	rm -f $(PROGRAM) $(OBJS)

cleanall:
	rm -f $(LIBTARGET) $(OBJS) index.html
	rm -R html

doxy:
	$(DOXY)
	ln -s html/index.html index.html

%.o: %.c $(HDRS)
	$(CC) $(CCOPTIONS) -c $<
