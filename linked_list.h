/*-----------------------------------------------------------
 * File:		linked_list.h
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-10-21
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Header for the linked list
 *-----------------------------------------------------------------------------
 */

#ifndef __linked_list_H__
#define __linked_list_H__

#include "utility.h"

typedef struct ll_node {
   void* data;
   struct ll_node* next;
}* LLNode;


typedef struct linked_list {
   LLNode firstNode;
   int length;
}* LinkedList;

int init_list(LinkedList list);

Bool add_data(LinkedList list, void* data);

void* get_at(LinkedList list, int element);

Bool remove_at(LinkedList list, int element);

Bool is_empty(LinkedList list);

int get_length(LinkedList list);

void** to_array(LinkedList list);

#endif