/*-----------------------------------------------------------
 * File:		file_utility.c
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Implementation of file utility library
 * ----------------------------------------------------------
 */

#include "file_utility.h"

void print_file_error()
{
	printf("\033[31mFATAL ERROR: file not found or no file specified\n\033[m");
}

int read_all_lines(string path, string** line_array)
{
	FILE* file = fopen (path, "r");
	int number_of_lines = 0;
	int last_length = 0;
	string buffer = 0x0;
	string* lines = 0x0;
	
	while (number_of_lines == 0 || last_length > 0)
	{
		buffer = new_string(MAX_LINE_LENGTH);
		fgets(buffer, MAX_LINE_LENGTH, file);
		last_length = strlen(buffer);
		if (last_length > 0)
		{
			lines = add_new_line(lines, number_of_lines, buffer);
			number_of_lines++;
		}
	}
	
	fclose(file);
	
	*line_array = lines;
	return number_of_lines;
}

Bool file_exists(string path)
{
	FILE* file = fopen (path, "r");
	
	if (file == 0x0)
		return False;
	
	fclose (file);
	return True;
}