/*-----------------------------------------------------------
 * File:		file_utility.h
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Header file for file utility library
 * ----------------------------------------------------------
 */

#ifndef ___FILE_UTILITY_H
#define ___FILE_UTILITY_H

#include <stdio.h> 
#include "utility.h"
 
#define MAX_LINE_LENGTH 256
 
void print_file_error();

int read_all_lines(string path, string** line_array);

Bool file_exists(string path);

#endif