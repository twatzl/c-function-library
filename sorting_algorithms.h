/*-----------------------------------------------------------
 * File:		sorting_algorithms.h
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Header for the sorting library
 * NOT WORKING YET
 *-----------------------------------------------------------------------------
 */
#ifndef ___SORTING_ALGORITHMS_H
#define ___SORTING_ALGORITHMS_H

/**
*** @file sorting_algorithms.h
*** @brief Description of sorting algorithms
*** @page Sorting Algorithms
*** Basically two functions to sort arrays of integer elements. Furthermore, a function
*** to initialize an array with a number of random elements.
*/

/**
*** Initializes an array with random elements.
*** @param array The array to be initialized.
*** @param length The length of the array.
*/
void init_random(int *array, unsigned long length);

/**
*** Sorts an array according to the bubble sort strategy.
*/
void bubble_sort(int *array, unsigned long length);

/**
*** Sorts an array according to the insertion sort strategy.
*/
void insertion_sort(int *array, unsigned long length);

void merge_sort(int *array, unsigned long length);

void print_array(int *array, unsigned long length);

int* merge_arrays(int *array1, unsigned long length1, int *array2, unsigned long length2);

/**
*** Sorts an array according to the quick sort strategy.
*/
void quick_sort(int *array, unsigned long length);

#endif
