/*-----------------------------------------------------------
 * File:		bst.h
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Header for the tree library
 * NOT WORKING YET
 *-----------------------------------------------------------------------------
 */
#ifndef ___BST_H
#define ___BST_H

typedef struct Node
{
	int value;
	struct Node* left;
	struct Node* right;
	struct Node* parent;
} TreeNode;

typedef struct Node *Bst;

/**
*** Initializes the binary search tree. Deletes all existing nodes in the tree
*** @return The number of deleted nodes
*/
int init_bst(Bst *bst);

/**
*** @return The depth of the BST
*/
int depth(const Bst bst);

/**
*** Adds a value to the BST
*/
void add(Bst *bst, int value);

/**
*** @return The value of the root element of the BST
*/
int root(Bst bst);

/**
*** @return The left subtree of the BST
*/
Bst left_subtree(Bst bst);

/**
*** @return The right subtree of the BST
*/
Bst right_subtree(Bst bst);

/**
*** Traverses the BST depth first by first returning the root, then traversing the left
*** subtree, then traversing the right subtree.
*** @param bst The BST to be traversed
*** @param elements Array of elements in the order how they are found during traversal
*** @param start Start index of elements wherer the function should start to add the found elements
*** @return Number of elements found during traversal
*/
int traverse_pre_order(Bst bst, int *elements, int start);

/**
*** Traverses the BST depth first by first traversing the left subtree, then adding the root,
*** then traversing the right subtree.
*** @param bst The BST to be traversed
*** @param elements Array of elements in the order how they are found during traversal
*** @param start Start index of elements wherer the function should start to add the found elements
*** @return Number of elements found during traversal
*/
int traverse_in_order(Bst bst, int *elements, int start);

/**
*** Traverses the BST depth first by first traversing the left subtree, then traversing the right
*** subtree and finally adding the root.
*** @param bst The BST to be traversed
*** @param elements Array of elements in the order how they are found during traversal
*** @param start Start index of elements wherer the function should start to add the found elements
*** @return Number of elements found during traversal
*/
int traverse_post_order(Bst bst, int *elements, int start);

/**
*** Deletes an element from the BST. If the element is a node with two children it
*** replaces the node with the largest element of the left subtree.
*** @param bst Binary search tree
*** @param element Element to be deleted
*/
void delete_element(Bst bst, int element);
#endif
