/*-----------------------------------------------------------
 * File:				linked_list.c
 * Author:				Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:			2013-10-21
 * Last Modified:		2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Implementation of the linked list
 *-----------------------------------------------------------------------------
 */

#include "linked_list.h"

LLNode get_node_at(LinkedList list, int element);

int init_list(LinkedList list)
{
	LLNode node;
	LLNode freeNode;
	int counter = 0;
	
	if (list == 0x0)
		list = (LinkedList) salloc(sizeof(struct linked_list));
	else
	{
		if (list->firstNode != 0x0)
		{
			node = list->firstNode;
			while (node != 0x0)
			{
				freeNode = node;
				node = node->next;
				free(freeNode);
				counter++;
			}
		}
		if (list->length != 0)
			list->length = 0;
	}
	return counter;
}

Bool add_data(LinkedList list, void* data)
{
	LLNode node = list->firstNode;
	LLNode newNode = (LLNode) salloc(sizeof(struct ll_node));
	
	newNode->data = data;
	newNode->next = 0x0;
	
	while (node->next != 0x0)
		node = node->next;
	
	node->next = newNode;
	(list->length)++;
	
	return True;
}

void* get_data_at(LinkedList list, int element)
{
	LLNode node = get_node_at(list, element);
	return (node == 0x0 ? 0x0 : node->data);
}

Bool remove_at(LinkedList list, int element)
{
	LLNode node;
	LLNode freeNode;
	
	if (element == 0)
	{
		if (list->firstNode == 0x0)
			return False;
		freeNode = list->firstNode;
		list->firstNode = list->firstNode->next;
	}
	else
	{
		node = get_node_at(list, element - 1);
		if (node == 0x0 || node->next == 0x0)
			return False;
		
		freeNode = node->next;
		node->next = node->next->next;
	}
	free(freeNode);
	(list->length)--;
	return True;
}

Bool is_empty(LinkedList list)
{
	if (list->length == 0)
		return True;
	else
		return False;
}

int get_length(LinkedList list)
{
	return list->length;
}

void** to_array(LinkedList list)
{
	int length = list->length;
	int counter = 0;
	void** array = salloc(sizeof(void*) * length);
	
	LLNode node = list->firstNode;
	
	while (node != 0x0)
	{
		array[counter] = node->data;
		counter++;
	}
	return array;
}

//--------------------------------------------- Private Functions -------------------------------------

LLNode get_node_at(LinkedList list, int element)
{
	LLNode node = list->firstNode;
	int counter = 0;
	
	if (element >= list->length)
		return 0x0;
	
	while (counter <= element)
		node = node->next;
	
	return node;
}