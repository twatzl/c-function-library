/*-----------------------------------------------------------
 * File:		bst.c
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Implementation of the tree library
 * NOT WORKING YET
 *-----------------------------------------------------------------------------
 */

#include "general.h"

#include "stdlib.h"
#include "stdio.h"
#include "bst.h"
#include "string.h"

#define EMPTY 0x0

void delete_node(Node **node);
int add_node(Bst *bst, int value);
bool node_has_children(const Bst bst);
Bst* ptr_left_subtree(Bst bst);
Bst* ptr_right_subtree(Bst bst);
int larger_value(int a, int b);
int compare(int a, int b);
void* salloc(size_t size);
void add_value(int** array, int* length, int value);
void append_left(Bst* tree, Bst add);
void append_right(Bst* tree, Bst add);

int init_bst(Bst *bst)
{
	Bst root = *bst;
	int number_of_deleted_nodes = 1;

	if (root == EMPTY)
		return 0;
	
	if (left_subtree(root) != EMPTY)
	{
		if (!node_has_children(left_subtree(root)))
		{
			delete_node(ptr_left_subtree(root));
			root->left = EMPTY;
			number_of_deleted_nodes++;
		}
		else
		{
			number_of_deleted_nodes += init_bst(ptr_left_subtree(root));
		}
	}
	if (right_subtree(root) != EMPTY)
	{
		if (!node_has_children(right_subtree(root)))
		{
			delete_node(ptr_right_subtree(root));
			root->right = EMPTY;
			number_of_deleted_nodes++;
		}
		else
		{
			number_of_deleted_nodes += init_bst(ptr_right_subtree(root));
		}
	}
	
	*bst = EMPTY;
	
	return number_of_deleted_nodes;
}

int depth(Bst bst)
{
	if (bst == EMPTY)
		return 0;
	
	if (!node_has_children(bst))
	{
		return 1;
	}
	else
	{
		if (left_subtree(bst) == EMPTY)
		{
			return depth(right_subtree(bst)) + 1;
		}
		else if (right_subtree(bst) == EMPTY)
		{
			return depth(left_subtree(bst)) + 1;
		}
		else
		{
			return larger_value(depth(left_subtree(bst)), depth(right_subtree(bst))) + 1;
		}
	}
}


void add(Bst *bst, int value)
{
	add_node(bst, value);
}

int root(const Bst bst)
{
	if(bst != EMPTY)
		return bst->value;
	else
		return -1;
}

Bst left_subtree(Bst bst)
{
	return bst->left;
}

Bst right_subtree(Bst bst)
{
	return bst->right;
}

int traverse_pre_order(Bst bst, int *elements, int start)
{
	int length = start;
	
	//printf("%d\n", start);
	
	if (bst == EMPTY)
		return 0;
	
	elements[length] = bst->value;
	//printf("%d\n", bst->value);
	length++;
	//if (left_subtree(bst) != EMPTY)
	length += traverse_pre_order(left_subtree(bst), elements, length);
	
	//if (right_subtree(bst) != EMPTY)
	length += traverse_pre_order(right_subtree(bst), elements, length);
	
	return length - start;
}

int traverse_in_order(Bst bst, int *elements, int start)
{
	int length = start;
	
	if (bst == EMPTY)
		return 0;
	
	length += traverse_in_order(left_subtree(bst), elements, length);
	elements[length] = bst->value;
	length++;
	length += traverse_in_order(right_subtree(bst), elements, length);
	
	return length - start;
}

int traverse_post_order(Bst bst, int *elements, int start)
{
	int length = start;
	
	if (bst == EMPTY)
		return 0;
	
	length += traverse_post_order(left_subtree(bst), elements, length);
	length += traverse_post_order(right_subtree(bst), elements, length);
	elements[length] = bst->value;
	length++;
	
	return length - start;
}

int print_in_order(Bst bst, int start)
{
	int length = start;
	
	if (bst == EMPTY)
		return 0;
	
	length += print_in_order(left_subtree(bst), length);
	printf("%d ", bst->value);
	length++;
	length += print_in_order(right_subtree(bst), length);
	
	return length - start;
}

void delete_element(Bst bst, int element)
{
	
	if (bst == EMPTY || !node_has_children(bst))
		return;
	//printf("\n%d\n", element);
	//print_in_order(bst, 0);
	//printf("\n");
	
	if (left_subtree(bst) != EMPTY)
	{
		if (left_subtree(bst)->value == element)
		{
			delete_node(&(bst->left));
		}
		else
			delete_element(bst->left, element);
	}
	
	if (right_subtree(bst) != EMPTY)
	{
		if (right_subtree(bst)->value == element)
		{
			delete_node(&(bst->right));
		}
		else
			delete_element(bst->right, element);
	}
}

//***************************Private Functions**********************************



void delete_node(TreeNode **node)
{
	Bst subtree = 0x0;
	if (left_subtree(*node) != EMPTY)
		append_left(&subtree, left_subtree(*node));
	
	if (right_subtree(*node) != EMPTY)
	{
		append_right(&subtree, right_subtree(*node));
	}
	free(*node);
	*node = subtree;
}

void append_right(Bst* tree, Bst add)
{
	if (add == EMPTY)
		return;
	if (*tree == EMPTY)
	{
		*tree = add;
		return;
	}
	
	if (right_subtree(*tree) == EMPTY)
	{
		(*tree)->right = add;
	}
	else
	{
		append_right(&((*tree)->right), add);
	}
}

void append_left(Bst* tree, Bst add)
{
	if (add == EMPTY)
		return;
	if (*tree == EMPTY)
	{
		*tree = add;
		return;
	}
	
	if (left_subtree(*tree) == EMPTY)
	{
		(*tree)->left = add;
	}
	else
	{
		append_left(&((*tree)->left), add);
	}
}

int add_node(TreeNode **root, int value)
{
	TreeNode *node = *root;
	
	if (node == EMPTY)
	{
		node = (TreeNode*) salloc(sizeof(TreeNode));
		node->value = value;
		node->left = EMPTY;
		node->right = EMPTY;
		node->parent = EMPTY;
		*root = node;
		return 1;
	}
	
	switch (compare(value, node->value))
	{
		case 0:
		case 1:
			if (add_node(ptr_right_subtree(node), value) == 1)
			{
				node->right->parent = node;
			}
			break;
			
		case -1:
			if (add_node(ptr_left_subtree(node), value) == 1)
			{
				node->left->parent = node;
			}
			break;
		
		default:
			return -1;
			break;
	}
	
	return 0;
}

bool node_has_children(const Bst bst)
{
	return !(left_subtree(bst) == EMPTY && right_subtree(bst) == EMPTY);
}

int larger_value(int a, int b)
{
	return a > b ? (a) : (b);
}

Bst* ptr_left_subtree(Bst bst)
{
	return &bst->left;
}

Bst* ptr_right_subtree(Bst bst)
{
	return &bst->right;
}

void* salloc(size_t size)
{
	void* ptr = malloc(size);
	if (ptr == 0x0)
	{
		fprintf(stderr, "no fucking memory anymore! goodbye");
		exit(1);
	}
	
	return ptr;
}

int compare(int a, int b)
{	
	if (a > b)
		return 1;
	else if (a < b)
		return -1;
	else
		return 0;
}

void print_array(int* array, int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

void add_value(int** array, int* length, int value)
{
	int* arr = (int*) salloc(sizeof(int) * (unsigned long) (*length) + 1);
	
	if (*length > 0)
	{
		memcpy(arr, *array, sizeof(int) * (unsigned long) (*length));
	}
	if (*array != EMPTY)
	{
		free(*array);
	}

	printf("%i\n", value);
	
	printf("*array:\n");
	print_array(*array, *length);
	printf("*arr:\n");
	print_array(arr, (*length) + 1);
	

	arr[*length] = value;
	printf("*arr:\n");
	print_array(arr, (*length) + 1);
	*array = arr;
	
	printf("*array:\n");
	print_array(*array, *length);
	printf("\n\n");
	(*length) += 1;
}