/*-----------------------------------------------------------
 * File:		utility.c
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Implementation of utility library
 * ----------------------------------------------------------
 */

#include "utility.h"

void* salloc(size_t size)
{
	void* ptr = malloc(size);
	if (ptr == 0x0)
	{
		fprintf(stderr, "No more memory aviable. Exiting now!");
		exit(1);
	}
	
	return ptr;
}

int power(int base, int exponent)
{
	int number = 1;
	for (int i = 0; i < exponent; i++)
		number *= base;
	
	return number;
}

double square(double number)
{
	return number * number;
}

double pythagoras(double a, double b)
{
	return sqrt(square(a) + square(b));
}

Bool is_number(char ch)
{
	if (ch >= '0' && ch <= '9')
		return True;
	
	return False;
}

void microsleep(int time)
{
	struct timespec ts;
	
	ts.tv_sec = 0;
	ts.tv_nsec = time * 1000;
	
	nanosleep(&ts, &ts);
}

/*void switch_items(void* ptr1, void* ptr2)
{
	void* tmp;
	tmp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = tmp;
}*/

string new_string(int length)
{
	string str = (string) salloc(sizeof(char)*length);
	
	if (length > 0)
		str[0] = '\0';
	
	return str;
}

int split_string(string str, char ch, string** return_array)
{
	int counter = 0;
	int number_of_splits = 0;
	char last_char = '\0';
	*return_array = (string*) salloc(sizeof(string*) * strlen(str));
	
	while (str[counter] != '\0')
	{
		if (str[counter] == ch)
		{
			str[counter] = '\0';
		}
		else if (last_char == '\0')
		{
			(*return_array)[number_of_splits] = &str[counter];
			number_of_splits++;
		}
		last_char = str[counter];
		counter++;
	}
	return number_of_splits;
}

void replace_chars(string str, char char_to_be_replaced, char char_to_replace)
{
	int counter = 0;
	
	while (str[counter] != '\0')
	{
		if (str[counter] == char_to_be_replaced)
			str[counter] = char_to_replace;
		
		counter++;
	}
}

string* add_new_line(string* array, int act_number_of_lines, string line)
{
	string* new_array = (string*) salloc(sizeof(string) * (act_number_of_lines + 1));
	new_array[act_number_of_lines] = line;
	if (array != 0x0)
	{
		memcpy(new_array, array, sizeof(string) * act_number_of_lines);
	}
	free(array);
	return new_array;
}

int convert_string_to_int(string str)
{
	int counter = 0;
	int buffer_counter = 0;
	int number = 0;
	string buffer = new_string(strlen(str));
	
	while (str[counter] != '\0')
	{
		if (is_number(str[counter]))
		{
			buffer[buffer_counter] = str[counter];
			buffer_counter++;
		}
		counter++;
	}
	buffer[buffer_counter] = '\0';
	
	for (int i = 0; i < buffer_counter; i++)
	{
		number += power(10, buffer_counter - i - 1) * (convert_char_to_int(buffer[i]));
	}
	
	return number;
}

string convert_int_to_string(int i)
{
	int counter = 0;
	int str_counter = 0;
	string str = 0x0;
	
	while (power(10, counter) < i)
		counter++;
	
	str = new_string(counter + 1);
	
	while (counter > 0)
	{
		str[str_counter] = convert_int_to_char( i / power(10, counter));
		counter--;
		str_counter++;
	}
	
	str[str_counter] = '\0';
	
	return str;
}

int convert_char_to_int(char ch)
{
	if (ch <= '0' || ch >= '9')
		return -1;
	
	return (int) ch - (int) '0';
}

char convert_int_to_char(int i)
{
	return (char) (i + (int) '0');
}