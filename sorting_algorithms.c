/*-----------------------------------------------------------
 * File:		sorting_algorithms.c
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Implementation of some sorting algorithms
 * NOT WORKING YET
 *-----------------------------------------------------------------------------
 */
#include "sorting_algorithms.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void* salloc(size_t size)
{
	void* ptr = malloc(size);
	if (ptr == 0x0)
	{
		exit(-1);
	}
	else
	{
		return ptr;
	}
}

void init_random(int *array, unsigned long length)
{
	srandom((int) time(0x0));
	for(int i = 0; i < (int) length; i++)
	{
		array[i] = random();
	}
}

void switch_elements(int *element1, int *element2)
{
	int tmp = *element2;
	*element2 = *element1;
	*element1 = tmp;
}

void check_and_change(int *element1, int *element2)
{
	if (*element1 > *element2)
	{
		switch_elements(element1, element2);
	}
}

void bubble_sort(int *array, unsigned long length)
{
	int unsorted_elements = length;
	while (unsorted_elements > 0)
	{
		for(int i = 0; i < unsorted_elements - 1; i++)
		{
			check_and_change(&array[i], &array[i+1]);
		}
		unsorted_elements--;
	}
}

void print_array(int *array, unsigned long length)
{
	for (int i = 0; i < (int) length; i++)
	{
		printf("%i\n", array[i]);
	}
}

void insertion_sort(int *array, unsigned long length)
{
	int sorted_area = 0;
	while(sorted_area < (int) length - 1)
	{
		if (array[sorted_area] > array[sorted_area + 1])
		{
			int search_at = sorted_area;
			int element = array[sorted_area + 1];
			while(array[search_at] > element && search_at > 0)
			{
				array[search_at + 1] = array[search_at];
				search_at--;
			}
			array[search_at] = element;
		}
		sorted_area++;
	}
}

int* merge_arrays(int *array1, unsigned long length1, int *array2, unsigned long length2)
{
	int *new_array = (int*) salloc(sizeof(int) * (length1 + length2));
	unsigned long counter1 = 0;
	unsigned long counter2 = 0;
	unsigned long new_counter = 0;
	
	while (counter1 < length1 || counter2 < length2)
	{
		if (counter2 >= length2 || (array1[counter1] <= array2[counter2] && counter1 < length1))
		{
			new_array[new_counter] = array1[counter1];
			counter1++;
		}
		else
		{
			new_array[new_counter] = array2[counter2];
			counter2++;
		}
		new_counter++;
	}
	
	//print_array(array1, length1);
	//printf("\n\n\n");
	//print_array(array2, length2);
	//scanf("\n");
	return new_array;
}

void merge_sort(int *array, unsigned long length)
{
	int* arr;
	int* arr1;
	int* arr2;
	unsigned long length1 = 0;
	unsigned long length2 = 0;
	
	
	if (length == 1)
	{
		return;
	}
	else
	{
		if ((length/2)*2 != length)
		{
			length1 = length/2 + 1;
			length2 = length/2;
			arr1 = &array[0];
			arr2 = &array[length/2 + 1];
		}
		else
		{
			length1 = length/2;
			length2 = length/2;
			arr1 = &array[0];
			arr2 = &array[length/2]; 
		}
		merge_sort(arr1, length1);
		merge_sort(arr2, length2);
		arr = merge_arrays(arr1, length1, arr2, length2);
		memcpy(array,arr, sizeof(int) * length);
		free(arr);
	}
}

void split_array(int* array, unsigned long length, int** array1, unsigned long *length1, int** array2, unsigned long *length2, int n)
{
  int *arr1 = (int*) salloc(sizeof(int) * length);
  int *arr2 = (int*) salloc(sizeof(int) * length);
  *length1 = 0;
  *length2 = 0;
  
	for(unsigned int i = 0; i < length; i++)
	{
		if (array[i] < n)
		{
			arr1[*length1] = array[i];
			*length1 = *length1 + 1;
		}
		else
		{
			arr2[*length2] = array[i];
			*length2 = *length2 + 1;
		}
	}
	*array1 = arr1;
	*array2 = arr2;
}

void quick_sort(int *array, unsigned long length)
{
 	int* arr;
	int* arr1;
	int* arr2;
	unsigned long length1 = 0;
	unsigned long length2 = 0;
	
	
	if (length == 1)
	{
		return;
	}
	else
	{
		split_array(array, length, &arr1, &length1, &arr2, &length2, array[0]);
		
		if (length1 == 0 || length2 == 0)
			split_array(array, length, &arr1, &length1, &arr2, &length2, array[length/2]);
		
		if (length1 == 0 || length2 == 0)
			split_array(array, length, &arr1, &length1, &arr2, &length2, array[length - 1]);
		
		if (length1 == 0 || length2 == 0)
			return;
		
		quick_sort(arr1, length1);
		quick_sort(arr2, length2);
		arr = merge_arrays(arr1, length1, arr2, length2);
		memcpy(array,arr, sizeof(int) * length);
		free(arr);
	}
}
