/*-----------------------------------------------------------
 * File:		term_utility.c
 * Author:		Tobias Watzl
 * Last Modified by:	Tobias Watzl
 * Create Date:		2013-05-12
 * Last Modified:	2013-10-21
 * ----------------------------------------------------------
 * Description:
 * Implementation of terminal utility library
 * ----------------------------------------------------------
 */

#include "term_utility.h" 

static struct termios orig_term_attr;

int get_terminal_dimension(enum terminal_dimension dimension)
{
	struct winsize ws;
	if (ioctl(0,TIOCGWINSZ,&ws)!=0)
	{
		fprintf(stderr,"TIOCGWINSZ:%s\n",strerror(errno));
		exit(-1);
	}
	//printf("row=%d, col=%d, xpixel=%d, ypixel=%d\n",
	//ws.ws_row,ws.ws_col,ws.ws_xpixel,ws.ws_ypixel);
	
	if (dimension == WIDTH)
		return ws.ws_col;
	
	return ws.ws_row;
}
 
struct termios disable_terminal_buffer()
{
    struct termios new_term_attr;

    /* set the terminal to raw mode */
    tcgetattr(fileno(stdin), &orig_term_attr);
    memcpy(&new_term_attr, &orig_term_attr, sizeof(struct termios));
    new_term_attr.c_lflag &= ~(ECHO|ICANON);
    new_term_attr.c_cc[VTIME] = 0;
    new_term_attr.c_cc[VMIN] = 0;
    tcsetattr(fileno(stdin), TCSANOW, &new_term_attr);
	
	return orig_term_attr;
}

void set_terminal_attrib(struct termios attr)
{
	tcsetattr(fileno(stdin), TCSANOW, &attr);
}

void enable_terminal_buffer()
{
	tcsetattr(fileno(stdin), TCSANOW, &orig_term_attr);
}